#include<iostream>
using namespace std;

float Tocm(float );							//Function prototype of Function change inches to cm. 
float size(float , float , float );			//Function prototype of Function calculate size of brick
float total(float ,float );					//Function prototype of Function collect price 
float dis10(float);							//Function prototype of Function discount 10% for member 
float dis20(float [], float );				//Function prototype of Function discount 20% for member	


int main(){
	float width, height, length;  			//Variable for measure in inches
	float nwidth, nheight, nlength;			//Variable for measure in cm.	
	float p[3] = {0,0,0};					//Variable for price of brick
	int brick ;								//Variable for kind of brick
	int count[3] = {0,0,0};					//Variable for counter of 3kind of brick
	float surface, price, total_price=0;	//surface for surface area, price for price of brick, total_price for total price
	float distance,del_ser;					//distance for distance from store to customer's home, del_ser for delivery service charge
	int member;								//member for customer answer

	cout << "*************************************************Welcome to KNE's BRICK*************************************************\n"; 	//show message
do{
	cout << "\nselect brick (1 for clay bricks, 2 for Gold bricks, 3 for Unobtainium Bricks ,-1 for Next stage)...";	//show message
	cin >> brick;		//store value
	
	switch(brick){ 		
		case 1:													
			cout <<"Clay Bricks\n";				//show type of brick
			count[0]++;							//counter clay brick
			break;
		case 2:
			cout <<"Gold bricks\n";				//show type of brick
			count[1]++;							//counter clay brick
			break;
		case 3:
			cout <<"Unobtainium Brick\n";		//show type of brick
			count[2]++;							//counter clay brick
			break;
		default :
			cout <<"\n";
			continue;
		
	}
	
	cout << "Enter width in inches ";			//show message
	cin >> width;								//stroe value
	nwidth = Tocm(width);						//call function

	cout << "Enter height in inches ";			//show message
	cin >> height;								//stroe value
	nheight = Tocm(height);						//call function

	cout << "Enter length in inches ";			//show message
	cin >> length;								//stroe value
	nlength = Tocm(length);  					//call function
	
	cout << "surface area is " << size(nwidth, nheight, nlength) << " square centimeter \n"; 		//show message
	surface = size(nwidth, nheight, nlength);														//call function

	if(brick == 1){								//clay brick
	price = surface * 10;						//price
	p[0] = p[0] + price;						//total price of clay brick
	}
	else if(brick == 2){						//gold brick
	price = surface * 30;						//price
	p[1] = p[1] + price;						//total price of gold brick
	}
	else if(brick == 3){						//Unobtainium brick
	price = surface * 90;						//price
	p[2] = p[2] + price;						//total price of Unobtainium brick
	}

	cout << "Brick price is " << price << " Baht\n";		//show message
	total_price = total(total_price,price);					//call function
	
}while(brick!=-1);			
	
	cout << "*****************************************************************";		//show message
	cout << "\nDelivery Service distance...";											//show message
	cin >> distance;																	//store value
	if(distance <= 5){								
	del_ser=0;																		
	}else if(distance > 5 && distance <= 20){
		del_ser = 100*distance;
	}else if(distance > 20){
		del_ser = 150*distance;
	}
	cout << "*****************************************************************"; 		//show message
	cout << "\nBrick price " << total_price <<" Baht\n";								//show message
	cout << "Delivery service charge " << del_ser << " Baht\n";							//show message
	total_price = total_price + del_ser;												//calculate total price
	cout << "***Total " << total_price <<" Baht***\n";									//show message
	cout << "*****************************************************************";		//show message
	cout << "\nAre you member ? (1 for Yes,I am ,2 for No,I don't)...";					//show message
	cin >> member;																		//store value
	
	switch(member){
		case 1:																					//case 1: for member
			if(count[0] >= 1 && count[1] >= 1 && count[2] >=1){									//discount 20% for member who buy 3kind of brick	
				cout << "Get a 20% discount on the cheapest brick!\n";							//show message
				cout << "***Total " <<dis20(p, total_price) << " Baht***\n";					//show message and call function 
				cout << "****************************THANK YOU****************************";	//show message
			}else if (count[0] < 1 || count[1] < 1 || count[2] < 1){							//discount 10% for all member
				cout << "Get a 10% discount\n";													//show message
				cout << "***Total " << dis10(total_price) << " Baht***\n";						//show message and call function
				cout << "****************************THANK YOU****************************";	//show message
			}
			break;
		case 2:
				cout << "***Total " << total_price <<" Baht***\n";								//show message and call function
				cout << "****************************THANK YOU****************************"; 	//show message
			break; 			
	}
	return 0;
}

float Tocm(float c){							//function change inches to cm. 
	return c*2.54;
}

float size(float a, float b, float c){			//Function calculate size of brick
	return (a*b*2) + (a*c*2) + (b*c*2);
}
	
float total(float total,float price){			//Function collect price 
	return total+price;
}

float dis10(float total){						//Function discount 10% for member 
	return (total*90)/100;
}


float dis20(float p[], float total ){			//Function discount 20% for member	
	float min, dis; 
	for(int i=0; i<2; i++){
		for(int j=0; j<i; j++)
		if(p[j] < p[j+1]){
			min = p[j];	
		}
	}
	dis = (min*20)/100; 
	return total - dis;
}












